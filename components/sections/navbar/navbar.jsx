import React from 'react'
import PropTypes from 'prop-types'
import EDgrid from '../../ed-grid/ed-grid'
import EDcontainer from '../../ed-grid/ed-container'
import EDitem from '../../ed-grid/ed-item'
import Avatar from '../../avatar/avatar'

const Navbar = ({
  className,
}) => (
  <div className={`navbar ${className}`}>
    <EDgrid sm={2}>
      <ul className="nav">
        <li>Home</li>
        <li>Account</li>
      </ul>
      <EDcontainer>
        <EDitem className="flex s-cross-center s-main-end">
          <Avatar src="https://randomuser.me/api/portraits/men/86.jpg" />
        </EDitem>
      </EDcontainer>
    </EDgrid>
  </div>
)

Navbar.defaultProps = {
  className: '',
}

Navbar.propTypes = {
  className: PropTypes.string,
}

export default Navbar
