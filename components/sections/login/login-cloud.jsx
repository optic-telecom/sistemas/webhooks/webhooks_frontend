import React from 'react'
import PropTypes from 'prop-types'
import Form from '../../form/form'
import Button from '../../button/button'
import FormGroup from '../../form/form-group'
import Input from '../../form/input'
import Label from '../../form/label'
import AuthProvider from '../../../services/AuthProvider'

const LoginCloud = ({
  className, isOpen,
}) => {
  const signIn = (e) => {
    e.preventDefault()

    const args = {
      email: e.target.email.value,
      password: e.target.password.value,
    }

    AuthProvider.signIn(args)

    e.target.reset()
  }

  return isOpen && (
  <div className={`login-cloud ${className}`}>
    <Form className="s-pt-1" onSubmit={signIn}>
      <FormGroup>
        <Label>Correo Electrónico</Label>
        <Input type="email" name="email" required />
      </FormGroup>
      <FormGroup>
        <Label>Contraseña</Label>
        <Input type="password" name="password" required />
      </FormGroup>
      <FormGroup className="s-main-end s-pt-1">
        <Button full type="submit">Log In</Button>
      </FormGroup>
    </Form>
  </div>
  )
}

LoginCloud.defaultProps = {
  className: '',
  isOpen: false,
}

LoginCloud.propTypes = {
  className: PropTypes.node,
  isOpen: PropTypes.bool,
}

export default LoginCloud
