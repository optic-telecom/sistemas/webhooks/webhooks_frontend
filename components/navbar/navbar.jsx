import React from 'react'
import PropTypes from 'prop-types'

const Navbar = ({
  className, children,
}) => (
  <div className={`navbar ${className}`}>
    {children}
  </div>
)

Navbar.defaultProps = {
  children: '',
  className: '',
}

Navbar.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
}

export default Navbar
