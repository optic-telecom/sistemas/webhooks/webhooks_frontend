import React from 'react'
import PropTypes from 'prop-types'

const Typography = ({
  tag, className, children, variant,
}) => {
  switch (tag) {
    case 'h1':
      return <h1 className={`${className} ${variant}`}>{children}</h1>
    case 'h2':
      return <h2 className={`${className} ${variant}`}>{children}</h2>
    case 'h3':
      return <h3 className={`${className} ${variant}`}>{children}</h3>
    case 'h4':
      return <h4 className={`${className} ${variant}`}>{children}</h4>
    case 'h5':
      return <h5 className={`${className} ${variant}`}>{children}</h5>
    case 'h6':
      return <h6 className={`${className} ${variant}`}>{children}</h6>
    case 'span':
      return <span className={`${className} ${variant}`}>{children}</span>
    case 'strong':
      return <strong className={`${className} ${variant}`}>{children}</strong>
    default:
      return <p className={`${className} ${variant}`}>{children}</p>
  }
}

Typography.defaultProps = {
  tag: 'p',
  className: '',
  children: '',
  variant: '',
}

Typography.propTypes = {
  tag: PropTypes.oneOf(['p', 'span', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
  className: PropTypes.string,
  children: PropTypes.node,
  variant: PropTypes.string,
}

export default Typography
