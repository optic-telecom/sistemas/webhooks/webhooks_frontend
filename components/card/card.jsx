import React from 'react'
import PropTypes from 'prop-types'

const Card = ({
  children, className,
}) => (
  <div className={`card ${className}`}>
    {children}
  </div>
)

Card.defaultProps = {
  className: '',
  children: '',
}

Card.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
}

export default Card
