import React from 'react'
import PropTypes from 'prop-types'

const Button = ({
  children, className, color, size, onClick, type, disabled, full,
}) => (
  <button
    type={type}
    className={`button ${className} button--${color} button--${size}
		${disabled ? 'disabled' : ''} ${full ? 'full' : ''}`}
    onClick={!disabled ? onClick : () => {}}
  >
    {children}
  </button>
)

Button.defaultProps = {
  children: '',
  className: '',
  color: 'primary',
  size: 'normal',
  type: 'button',
  full: '',
  disabled: false,
  onClick: () => {},
}

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  full: PropTypes.bool,
  color: PropTypes.oneOf(['primary', 'success', 'danger', 'warn']),
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
  type: PropTypes.oneOf(['button', 'submit']),
  onClick: PropTypes.func,
}

export default Button
