import React from 'react'
import PropTypes from 'prop-types'

const EDcontainer = ({
  children, className,
}) => (
  <div className={`ed-container ${className}`}>{children}</div>
)

EDcontainer.defaultProps = {
  children: '',
  className: '',
}

EDcontainer.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
}

export default EDcontainer
