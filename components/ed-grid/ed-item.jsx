import React from 'react'
import PropTypes from 'prop-types'

const EDitem = ({
  children, className, container,
}) => (
  <div className={`ed-item ${container ? 'ed-container' : ''} ${className}`}>{children}</div>
)

EDitem.defaultProps = {
  children: '',
  className: '',
  container: false,
}

EDitem.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  container: PropTypes.bool,
}

export default EDitem
