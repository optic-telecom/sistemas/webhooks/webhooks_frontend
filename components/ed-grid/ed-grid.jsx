import React from 'react'
import PropTypes from 'prop-types'

const EDgrid = ({
  children, className, sm, md, lg,
}) => (
  <div
    className={`
			ed-grid ${className} ${sm ? `s-grid-${sm}` : ''} ${md ? `m-grid-${md}` : ''} ${lg ? `lg-grid-${lg}` : ''}
	`}
  >
    {children}
  </div>
)

EDgrid.defaultProps = {
  children: '',
  className: '',
  sm: false,
  md: false,
  lg: false,
}

EDgrid.propTypes = {
  sm: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  md: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  lg: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
  children: PropTypes.node,
  className: PropTypes.string,
}

export default EDgrid
