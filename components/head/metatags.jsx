import React from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'

const Metatags = ({
  title,
}) => (
  <Head>
    <title>{title ? `${title} | ${process.env.SITE_TITLE}` : process.env.SITE_TITLE}</title>
    <link rel="icon" href="/favicon.ico" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" />
  </Head>
)

Metatags.defaultProps = {
  title: '',
}

Metatags.propTypes = {
  title: PropTypes.string,
}

export default Metatags
