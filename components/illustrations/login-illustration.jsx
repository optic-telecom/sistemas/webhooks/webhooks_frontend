import React from 'react'

const LoginIllustration = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="756" height="800" viewBox="0 0 756 800">
    <defs>
      <clipPath id="clip-path">
        <rect
          id="Rectangle_18"
          dataName="Rectangle 18"
          width="756"
          height="800"
          transform="translate(524)"
          fill="#fff"
          stroke="#707070"
          strokeWidth="1"
          opacity="0"
        />
      </clipPath>
    </defs>
    <g id="Mask_Group_1" dataName="Mask Group 1" transform="translate(-524)" clipPath="url(#clip-path)">
      <g id="Group_39" dataName="Group 39">
        <path
          id="Path_121"
          dataName="Path 121"
          d="M551.018-43.581C420.232,223.12,514.248,561.2,705.587,677.16s860.294,23.763,860.294,23.763-4.75-324.153,
					0-334.1-68.872-389.779-73.622-397.734-455.982-77.558-536.73-75.569S681.8-310.283,551.018-43.581Z"
          transform="translate(77.295 274.291) rotate(-11)"
          fill="#3f4bee"
          opacity="0.16"
        />
        <path
          id="Path_1"
          dataName="Path 1"
          d="M548.73-42.838c-63.236,248.866-139.88,517.305,271.3,779.237s699.471-30.685,699.471-30.685-4.544-325.915,
					0-335.913-65.88-391.9-70.424-399.9-436.174-77.98-513.413-75.98S611.966-291.7,548.73-42.838Z"
          transform="translate(127 63)"
          fill="#3f51b5"
        />
      </g>
    </g>
  </svg>

)

export default LoginIllustration
