import React from 'react'
import PropTypes from 'prop-types'
import Typography from '../typography/typography'
import Logotype from '../../assets/images/logotype.svg'

const Logo = ({
  className, withName,
}) => (
  <div className={`logo s-cross-center ${className}`}>
    <img src={Logotype} alt="Multifiber Hooks" />
    {
			withName && (
				<Typography tag="h1" variant="title">
  Multifiber
  {' '}
  <Typography tag="strong">Webhooks</Typography>
				</Typography>
			)
		}
  </div>
)

Logo.defaultProps = {
  className: '',
  withName: false,
}

Logo.propTypes = {
  className: PropTypes.string,
  withName: PropTypes.bool,
}

export default Logo
