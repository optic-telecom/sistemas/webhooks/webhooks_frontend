import React from 'react'
import PropTypes from 'prop-types'

const FormGroup = ({ children, className, ...rest }) => (
  <div className={`form-group ${className}`} {...rest}>
    {children}
  </div>
)

FormGroup.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  rest: PropTypes.node,
}

FormGroup.defaultProps = {
  className: '',
  children: '',
  rest: null,
}

export default FormGroup
