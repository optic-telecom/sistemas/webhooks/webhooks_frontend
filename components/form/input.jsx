import React from 'react'
import PropTypes from 'prop-types'

const Input = ({
  type, onChange, name, required, className, placeholder, options, ...rest
}) => (
  type === 'select'
    ? (
      <select onChange={onChange} name={name}>
        <option value="">{placeholder}</option>
        {
					options.map((option) => <option key={option.value} value={option.value}>{ option.label }</option>)
				}
      </select>
    )
    : (
      <input
        type={type}
        name={name}
        onChange={onChange}
        required={required}
        {...rest}
        className={`input ${className}`}
      />
    )
)

Input.defaultProps = {
  type: 'text',
  name: '',
  required: false,
  className: '',
  placeholder: '',
  options: [],
  rest: {},
  onChange: () => {},
}

Input.propTypes = {
  type: PropTypes.oneOf(['text', 'email', 'select', 'date', 'number', 'password']),
  name: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  placeholder: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object),
  className: PropTypes.string,
  rest: PropTypes.node,
}

export default Input
