import React from 'react'
import PropTypes from 'prop-types'

const Label = ({
  children, className, htmlFor, ...rest
}) => (
  <label className={`label ${className}`} {...rest} htmlFor={htmlFor}>
    {children}
  </label>
)

Label.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  rest: PropTypes.node,
  htmlFor: PropTypes.string,
}

Label.defaultProps = {
  className: '',
  children: '',
  rest: null,
  htmlFor: '',
}

export default Label
