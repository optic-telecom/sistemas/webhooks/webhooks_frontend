import React from 'react'
import PropTypes from 'prop-types'

const Form = ({ children, className, ...rest }) => (
  <form className={`form ${className}`} {...rest}>
    {children}
  </form>
)

Form.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  rest: PropTypes.node,
}

Form.defaultProps = {
  className: '',
  children: '',
  rest: null,
}

export default Form
