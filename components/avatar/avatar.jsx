import React from 'react'
import PropTypes from 'prop-types'

const Avatar = ({
  src, alt, className,
}) => (
  <img src={src} alt={alt} className={`avatar ${className}`} />
)

Avatar.defaultProps = {
  className: '',
  src: '',
  alt: '',
}

Avatar.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string,
  alt: PropTypes.string,
}

export default Avatar
