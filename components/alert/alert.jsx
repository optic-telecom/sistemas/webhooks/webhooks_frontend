import React from 'react'
import PropTypes from 'prop-types'
import useAlert from '../../hooks/use-alert'

const Alert = ({
  className, color, children,
}) => {
  const [isOpen, handleAlert] = useAlert()

  const handler = (e) => {
    e.preventDefault()
    console.log('close')
    handleAlert(false)
  }

  return (
    <div className={`alert ${className} alert-${color} ${isOpen ? '' : 'hide'} s-mb-2`}>
      <div className="alert-close" role="none" onClick={handler} onKeyDown={handler}>x</div>
      {children}
    </div>
  )
}

Alert.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(['primary', 'danger', 'warning', 'success', 'info']),
  children: PropTypes.node,
}

Alert.defaultProps = {
  className: '',
  color: 'primary',
  children: '',
}

export default Alert
