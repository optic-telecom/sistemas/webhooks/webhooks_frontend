# Multifiber Webhook

![MultiFiber](https://11d78a2a6994884ab361-69bf5871bcc7584c29047fe7e8f10ecf.ssl.cf1.rackcdn.com/logos/cropped.jpg)

Single Page Application built on React.js + Next to manage the webhooks of the MultiFiber company.

## Install
```
$ yarn
```

## Run
```
$ yarn dev
```
