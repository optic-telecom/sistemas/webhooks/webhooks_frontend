import React, { useState, useMemo } from 'react'
import PropTypes from 'prop-types'

export const ModalContext = React.createContext()

const ModalProvider = (props) => {
  const [isOpen, handleOpenState] = useState(false)

  const value = useMemo(() => ([
    isOpen,
    handleOpenState,
  ]), [isOpen])

  return <ModalContext.Provider value={value} {...props} />
}

ModalProvider.propTypes = {
  props: PropTypes.node,
}

ModalProvider.defaultProps = {
  props: {},
}

export default ModalProvider
