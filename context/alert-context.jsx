import React, { useState, useMemo } from 'react'
import PropTypes from 'prop-types'

export const AlertContext = React.createContext()

const AlertProvider = (props) => {
  const [isOpen, handleOpenState] = useState(false)

  const value = useMemo(() => ([
    isOpen,
    handleOpenState,
  ]), [isOpen])

  return <AlertContext.Provider value={value} {...props} />
}

AlertProvider.propTypes = {
  props: PropTypes.node,
}

AlertProvider.defaultProps = {
  props: {},
}

export default AlertProvider
