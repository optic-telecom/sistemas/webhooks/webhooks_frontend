import React from 'react'
import { shallow } from 'enzyme'
import { test, expect } from 'jest'
import Home from '../pages/index'

test('renders <Home /> without crash', () => {
  const component = shallow(<Home />)
  expect(component).not.toBeNull()
})
