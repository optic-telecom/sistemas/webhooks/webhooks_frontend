module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    'implicit-arrow-linebreak': ['error', 'beside'],
    'max-len': ['error', { code: 120 }],
    'no-console': 'off',
    'no-tabs': ['error', { allowIndentationTabs: true }],
    semi: ['error', 'never'],
    'react/jsx-props-no-spreading': 'off',
		'react/jsx-indent': ['error', 'tab' | 2], // eslint-disable-line
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/button-has-type': 'off',
  },
  ignorePatterns: [
    '.next/', 'jest/', 'assets/', 'node_modules/', '*.config.js', '*.setup.js',
  ],
}
