module.exports = {
	coveragePathIgnorePatterns: ['/node_modules/', '/.next/', '/public/'],
	setupFilesAfterEnv: ['./jest.setup.js'],
	transform: {
		'^.+\\.jsx?$': './jest/transformer/custom-transformer.js'
	}
}
