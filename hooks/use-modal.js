import React from 'react'
import { ModalContext } from '../context/modal-context'

const useModal = () => {
  const context = React.useContext(ModalContext)
  if (!context) throw new Error('useModal() debe estar dentro de ModalContext')

  return context
}

export default useModal
