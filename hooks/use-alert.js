import React from 'react'
import { AlertContext } from '../context/alert-context'

const useAlert = () => {
  const context = React.useContext(AlertContext)
  if (!context) throw new Error('useAlert() debe estar dentro de AlertContext')

  return context
}

export default useAlert
