import HttpRequest from './HttpRequest'

export default class AuthProvider extends HttpRequest {
  static async signIn({ email, password }) {
    this.endpoint = '/signin'
    const data = await this.post({ email, password })
    return data
  }

  static async signUp({ email, password }) {
    this.endpoint = '/signup'
    const data = await this.post({ email, password })
    return data
  }
}
