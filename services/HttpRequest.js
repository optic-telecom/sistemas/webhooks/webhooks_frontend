import axios from 'axios'

export default class HttpRequest {
  constructor() {
    this.endpoint = ''
    this.result = {
      data: null,
      error: null,
      code: null,
    }
  }

  static async get(id = null) {
    try {
      let response = null
      if (id) {
        response = await axios.get(`${process.env.APP_URI}/${this.endpoint}/${id}`)
      } else {
        response = await axios.get(`${process.env.APP_URI}/${this.endpoint}`)
      }

      this.result = {
        data: response.data,
        error: null,
        code: response.statusCode,
      }

      return this.result
    } catch (err) {
      console.log(err.message)
      this.result = {
        data: id ? null : [],
        err: err.message,
        code: err.statusCode,
      }

      return this.result
    }
  }

  static async post(data) {
    console.log(data)
    console.log(this.endpoint)
    console.log(process.env.APP_URI)

    return {}
    /*
    try {
      const response = await axios.post(`${this.uri}/${this.endpoint}`, data)

      this.result = {
        data: response.data,
        error: null,
        code: response.statusCode,
      }

      return this.result
    } catch (err) {
      console.log(err.message)

      this.result = {
        data: null,
        err: err.message,
        code: err.statusCode,
      }

      return this.result
		}
		*/
  }
}
