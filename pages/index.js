import React, { useEffect } from 'react'
import Head from 'next/head'
import EDgrid from '../components/ed-grid/ed-grid'
import Card from '../components/card/card'
import Alert from '../components/alert/alert'
import Typography from '../components/typography/typography'
import withNavbar from '../components/hoc/with-navbar'
import withAlert from '../components/hoc/with-alert'
import useAlert from '../hooks/use-alert'

const Home = () => {
  const [isOpen, handleOpenAlert] = useAlert(true)

  useEffect(() => {
    handleOpenAlert(true)
  }, [])

  return (
    <div className="container">
      <Head>
        <title>Webhook | MultiFiber</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <EDgrid sm={3}>
        <Card>
          <Typography>Tarjeta 1</Typography>
          <Alert color="danger" isOpen={isOpen}>
            Hola mundo
          </Alert>
        </Card>
        <Card>
          <Typography>Tarjeta 2</Typography>
        </Card>
        <Card>
          <Typography>Tarjeta 3</Typography>
        </Card>
      </EDgrid>
    </div>
  )
}

const HomeWithNavbar = withAlert(withNavbar(Home))

export default HomeWithNavbar
