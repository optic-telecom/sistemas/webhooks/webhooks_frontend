import React from 'react'
import LoginIllustration from '../components/illustrations/login-illustration'
import EDcontainer from '../components/ed-grid/ed-container'
import EDitem from '../components/ed-grid/ed-item'
import Button from '../components/button/button'
import Alert from '../components/alert/alert'
import Card from '../components/card/card'
import Logo from '../components/image/logo'
import Form from '../components/form/form'
import Label from '../components/form/label'
import Input from '../components/form/input'
import FormGroup from '../components/form/form-group'
import LoginImage from '../assets/images/login-image.png'
import withAlert from '../hoc/with-alert'
import useAlert from '../hooks/use-alert'

import Typography from '../components/typography/typography'

const Login = () => {
  const [isOpen, handleAlert] = useAlert()

  const submit = (e) => {
    e.preventDefault()
    e.target.reset()
    handleAlert(true)
  }

  return (
    <div className="login-page">
      <div className="login-content">
        <EDcontainer className="login-header">
          <EDitem>
            <Logo withName />
          </EDitem>
        </EDcontainer>
        <EDcontainer className="s-cross-center">
          <EDitem className="m-5" />
          <EDitem className="m-30">
            <Card>
              <Form onSubmit={submit}>
                <EDcontainer>
                  <EDitem>
                    <FormGroup>
                      <Label>Usuario</Label>
                      <Input name="username" autoComplete="off" required />
                    </FormGroup>
                    <FormGroup>
                      <Label>Contraseña</Label>
                      <Input name="password" type="password" autoComplete="off" required />
                    </FormGroup>
                    <Alert isOpen={isOpen} color="danger">
                      Usuario/Contraseña no coinciden
                    </Alert>
                    <FormGroup>
                      <Button type="submit" full>Sign In</Button>
                    </FormGroup>
                    <FormGroup className="s-center s-mb-0">
                      <Typography tag="span" variant="caption" className="pointer">
                        He olvidado mi contraseña
                      </Typography>
                    </FormGroup>
                  </EDitem>
                </EDcontainer>
              </Form>
            </Card>
          </EDitem>
        </EDcontainer>
      </div>
      <div className="login-illustration">
        <img src={LoginImage} alt="Login" />
        <LoginIllustration />
      </div>
    </div>
  )
}

const LoginWithAlert = withAlert(Login)

export default LoginWithAlert
