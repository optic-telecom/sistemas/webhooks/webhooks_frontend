import React from 'react'
import PropTypes from 'prop-types'
import '../assets/scss/styles.scss'
import Metatags from '../components/head/metatags'

const App = ({
  Component, pageProps,
}) => (
  <div className="App">
    <Metatags />
    <Component {...pageProps} />
  </div>
)

App.propTypes = {
  Component: PropTypes.oneOfType([PropTypes.node, PropTypes.instanceOf(null)]),
  pageProps: PropTypes.oneOfType([PropTypes.node, PropTypes.object]),
}

App.defaultProps = {
  Component: '',
  pageProps: {},
}

export default App
