const withImage = require('next-images')

module.exports = withImage({
  env: {
    SITE_TITLE: 'Webhook | Multifiber',
    APP_URL: '',
  },
})
