import React from 'react'
import AlertProvider from '../context/alert-context'

const withAlert = (WrappedComponent) => ({ isOpen, ...props }) => (
  <>
    <AlertProvider>
      <WrappedComponent isOpen={isOpen} {...props} />
    </AlertProvider>
  </>
)

export default withAlert
