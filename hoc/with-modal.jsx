import React from 'react'
import ModalProvider from '../context/modal-context'

const withModal = (WrappedComponent) => (props) => (
  <ModalProvider>
    <WrappedComponent {...props} />
  </ModalProvider>
)

export default withModal
