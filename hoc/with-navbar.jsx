import React from 'react'
import Navbar from '../components/sections/navbar/navbar'

const withNavbar = (Component) => (props) => (
  <>
    <Navbar className="s-mb-2" />
    <Component {...props} />
  </>
)

export default withNavbar
